/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.apiconnector;

import com.google.gson.Gson;
import com.squareprolabs.apiresponse.SalesApiResponseHandler;
import com.squareprolabs.dto.CashBookDto;
import com.squareprolabs.dto.InvoiceDto;
import com.squareprolabs.dto.ItemDto;
import com.squareprolabs.dto.MaterialInDto;
import com.squareprolabs.dto.SaleDto;
import com.squareprolabs.dto.StockInDto;
import com.squareprolabs.dto.StockOutDto;
import com.squareprolabs.ui.LoginUI;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class APITester {
    
    public static void main(String[] args) {
        StockOutDto dto = new StockOutDto();
        List<ItemDto> items = new ArrayList<>();
        int i=1;
        while(i<4) {
            ItemDto itemDto = new ItemDto();
            itemDto.setItemId(i);
            itemDto.setItemName("ITEM" + i);
            itemDto.setCostPrice(new BigDecimal(0));
            itemDto.setQuantity(i*10);
            itemDto.setRetailPrice(BigDecimal.ZERO);
            itemDto.setUnitPrice(BigDecimal.ONE);
            itemDto.setUserName("Chathura");
            itemDto.setWholeSalePrice(BigDecimal.ZERO);
            
            items.add(itemDto);
            
            i++;
        }
        
        dto.setItemDtos(items);
        dto.setQuantity(3);
        dto.setSalesRepId(3);
        dto.setStatus("ADD");
        dto.setStockDate(new Date());
        dto.setStockId(1);
        dto.setStockOut(4); //agent stock 
        dto.setStockOutBalance(9);
        dto.setStockOutId(1);
        dto.setStockOutNo("001");
        dto.setStockTime(new Timestamp(System.currentTimeMillis()));
        dto.setStockType("sfg");
        dto.setUserId(1);
        
        APITester apiTester = new APITester();
        
        apiTester.addStockOutData(dto);
        
//        StockInDto dto = new StockInDto();
//        
//        List<ItemDto> items = new ArrayList<>();
//        int i=1;
//        while(i<3) {
//            ItemDto itemDto = new ItemDto();
//            itemDto.setItemId(i);
//            itemDto.setItemName("ITEM" + i);
//            itemDto.setCostPrice(new BigDecimal(0));
//            itemDto.setQuantity(i+1);
//            itemDto.setRetailPrice(BigDecimal.ZERO);
//            itemDto.setUnitPrice(BigDecimal.ONE);
//            itemDto.setUserName("Chathura");
//            itemDto.setWholeSalePrice(BigDecimal.ZERO);
//            
//            items.add(itemDto);
//            
//            i++;
//        }
//        
//        dto.setItemDtos(items);
//        dto.setQuantity(3);
//        dto.setSalesRepId(1);
//        dto.setStatus("ADD");
//        dto.setStockDate(new Date());
//        dto.setStockInBalance(9);
//        dto.setStockInId(1);
//        dto.setTotalPrice(new BigDecimal(100));
//        dto.setStockInNo("001");
//        dto.setStockTime(new Timestamp(System.currentTimeMillis()));
//        dto.setStockType("sfg");
//        dto.setUserId(1);
//        
//        APITester apiTester = new APITester();
//        
//        apiTester.addStockInData(dto);

//        SalesApiResponseHandler salesApiResponseHandler = new SalesApiResponseHandler();
//        String inv_no = salesApiResponseHandler.getInvoiceNumber();
//        
//        SaleDto dto = new SaleDto();
//        
//        List<ItemDto> items = new ArrayList<>();
//        int i=1;
//        while(i<3) {
//            ItemDto itemDto = new ItemDto();
//            itemDto.setItemId(i);
//            itemDto.setItemName("ITEM" + i);
//            itemDto.setCostPrice(new BigDecimal(0));
//            itemDto.setQuantity(i+1);
//            itemDto.setRetailPrice(BigDecimal.ZERO);
//            itemDto.setUnitPrice(BigDecimal.ONE);
//            itemDto.setUserName("admin");
//            itemDto.setWholeSalePrice(BigDecimal.ZERO);
//            
//            items.add(itemDto);
//            
//            i++;
//        }
//        
//        dto.setItemDtos(items);
//        dto.setCustomerId(1);
//        dto.setDiscount(BigDecimal.ONE);
//        dto.setSalesDate(new Date());
//        dto.setSalesNo("00001");
//        dto.setSalesQty(5);
//        dto.setSalesTime(new Timestamp(System.currentTimeMillis()));
//        dto.setSalesType("dddd");
//        dto.setUserName("admin");
//        
//        CashBookDto cashBookDto = new CashBookDto();
//        cashBookDto.setBalance(new BigDecimal(100));
//        cashBookDto.setCashBookDescription("Sales");
//        cashBookDto.setCashDate(new Date());
//        cashBookDto.setCredit(new BigDecimal(100));
//        cashBookDto.setDebit(BigDecimal.ZERO);
//        
//        InvoiceDto invoiceDto = new InvoiceDto();
//        invoiceDto.setInvoiceNumber(inv_no);
//        invoiceDto.setInvoiceType("CASH");
//        invoiceDto.setPaymentType("CASH");
//        invoiceDto.setUserName("admin");
//        invoiceDto.setInvoiceTotal(new BigDecimal(120));
//        invoiceDto.setBalance(new BigDecimal(100));
//        invoiceDto.setInvoiceBalance(new BigDecimal(100));
//        invoiceDto.setPaidAmount(new BigDecimal(100));
//                
//        dto.setInvoiceDto(invoiceDto);
//        dto.setCashBookDto(cashBookDto);
//            
//        APITester apiTester = new APITester();
//        
//        apiTester.addSaleData(dto);
    }
    
    public SaleDto[] addSaleData(SaleDto saleDto) {
        try {
            String performRequest = ApiConnector.performRequest(saleDto, "/sale/addSale", "POST");
            System.out.println("post = " + performRequest);
            SaleDto[] returnDto = new Gson().fromJson(performRequest, SaleDto[].class);
            
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public StockOutDto[] addStockOutData(StockOutDto stockOutDto) {
        try {
            String performRequest = ApiConnector.performRequest(stockOutDto, "/stockOut/saveStockOut", "POST");
            System.out.println("post = " + performRequest);
            StockOutDto[] returnDto = new Gson().fromJson(performRequest, StockOutDto[].class);
            
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public StockInDto[] addStockInData(StockInDto stockInDto) {
        try {
            String performRequest = ApiConnector.performRequest(stockInDto, "/stockIn/saveStockIn", "POST");
            System.out.println("post = " + performRequest);
            StockInDto[] returnDto = new Gson().fromJson(performRequest, StockInDto[].class);
            
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
