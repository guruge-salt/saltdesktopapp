/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.ui;

import com.squareprolabs.apiresponse.CustomerApiHandler;
import com.squareprolabs.apiresponse.ProductApiResponseHandler;
import com.squareprolabs.apiresponse.SalesApiResponseHandler;
import com.squareprolabs.apiresponse.UserApiResponseHandler;
import com.squareprolabs.core.ProductItem;
import com.squareprolabs.core.StringTag;
import com.squareprolabs.dto.BankBookDto;
import com.squareprolabs.dto.CashBookDto;
import com.squareprolabs.dto.ChequeDataDto;
import com.squareprolabs.dto.CustomerDto;
import com.squareprolabs.dto.InvoiceDto;
import com.squareprolabs.dto.ItemDto;
import com.squareprolabs.dto.SaleAgentItemDto;
import com.squareprolabs.dto.SaleDto;
import com.squareprolabs.dto.UserDto;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class Sales extends javax.swing.JPanel implements ChequeDataObserver {

    /**
     * Creates new form Sales
     */
    private List<ItemDto> itemList;
    private CustomerDto[] customerList;
    ItemDto selectedDto;
    private int selectedIndex;
    private UserDto[] salesRepList;
    private BigDecimal totalPrice;
    private BigDecimal netSalesPrice;
    private int totalQty;
    private BigDecimal totalSalesValue = new BigDecimal(BigInteger.ZERO);
    private BigDecimal discountAmount;
    private BigDecimal balance = new BigDecimal(BigInteger.ZERO);
    private BigDecimal cashAmount;
    private BigDecimal chequeAmount;
    private BigDecimal totalBalance;
    private BigDecimal creditAmount;
    private LinkedHashMap<Integer, ItemDto> salesItemMap;
    private static final String solve = "Solve";
    private UserDto loggedInUser;
    private String bankName;
    private String chequeNumber;
    private BigDecimal bankAmount;
    private String invoiceNumber;
    private String selectedSalesRepName;
    private boolean isFirstTimeSalesRepSelection = false;
    private DefaultListModel<ProductItem> model;

    public Sales() {
        initComponents();
        loadData();
        salesItemMap = new LinkedHashMap<>();
        setTableCellListener();
        salesRepSelection.requestFocus();
        initialDisableFields();
    }

    public Sales(UserDto loggedInUser) {
        initComponents();
        this.loggedInUser = loggedInUser;
        loadData();
        salesItemMap = new LinkedHashMap<>();
        setTableCellListener();
        salesRepSelection.requestFocus();
        initialDisableFields();
    }

    private void setTableCellListener() {
        AbstractAction action = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TableCellListener tcl = (TableCellListener) e.getSource();

                System.out.println("Cell edit old val: " + tcl.getOldValue());
                System.out.println("Cell edit new val: " + tcl.getNewValue());
                try {
                    int oldVal = Integer.parseInt(String.valueOf(tcl.getOldValue()));
                    int newVal = Integer.parseInt(String.valueOf(tcl.getNewValue()));
                    int row = tcl.getRow();

                    salesTable.setValueAt(Integer.parseInt(String.valueOf(tcl.getNewValue())), row, 1);

                    BigDecimal unitPrice = new BigDecimal(String.valueOf(salesTable.getValueAt(row, 2)));
                    BigDecimal discount = new BigDecimal(String.valueOf(salesTable.getValueAt(row, 4)));

                    if (validateQty(newVal)) {
                        int updatableQty = 0;

                        if (newVal > oldVal) {
                            updatableQty = newVal - oldVal;
                            editQtyOfListItem(updatableQty, false);
                        } else {
                            updatableQty = oldVal - newVal;
                            editQtyOfListItem(updatableQty, true);
                        }

                        setNewQtyOfSalesItemMap(selectedDto.getItemId(), newVal);
                        performCalculationsOnCellUpdate(newVal, unitPrice, discount);
                        updateSalesValuesInTable(row);
                        setSalesSummary();
                        qtyTxt.requestFocus();
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Please add valid qty", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        };

        TableCellListener listener = new TableCellListener(salesTable, action);
    }

    private void loadData() {
        SalesApiResponseHandler handler = new SalesApiResponseHandler();
        invoiceNumber = handler.getInvoiceNumber();
        invoiceNumberTxt.setText(invoiceNumber);

        CustomerApiHandler customerHandler = new CustomerApiHandler();
        customerList = customerHandler.getCustomerData();
        populateCustomers(customerList);

        UserApiResponseHandler userApiHandler = new UserApiResponseHandler();
        salesRepList = userApiHandler.getSalesRepData();
        populateSalesReps(salesRepList);

        flushTable();
    }

    private void loadAgentItems(int id, Date agentStockDate) {
        try {
            SalesApiResponseHandler handler = new SalesApiResponseHandler();
            SaleAgentItemDto agentItemDto = new SaleAgentItemDto();
            agentItemDto.setAgentStockDate(agentStockDate);
            agentItemDto.setId(id);
            itemList = handler.getAgentItems(agentItemDto);
            if (itemList.size() != 0) {
                populateProducts(itemList);
                initialStartUpAddSale();

                if (isFirstTimeSalesRepSelection) {
                    qtyTxt.requestFocus();
                }

                if (isFirstTimeSalesRepSelection == false) {
                    customerSelection.requestFocus();
                    isFirstTimeSalesRepSelection = true;
                }
            } else {
                JOptionPane.showMessageDialog(null, "The agent you selected have no items to sell, please do stock out first", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void populateProducts(List<ItemDto> itemList) {
        model = new DefaultListModel<>();

        for (ItemDto dto : itemList) {
            System.out.println("dto======================== = " + dto.getQuantity());
            Icon icon = new ImageIcon(new ImageIcon(getClass().getClassLoader().getResource("com/squareprolabs/images/productSmall.png")).getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
            model.addElement(new ProductItem(dto.getItemName(), dto.getQuantity(), icon));
        }
        productItemList.setCellRenderer(new ProductListItem());
        productItemList.setModel(model);
    }

    private void populateCustomers(CustomerDto[] customerList) {
        if (customerList.length == 0) {
            customerSelection.addItem("Select Customer");
        } else {
            customerSelection.removeAllItems();

            for (CustomerDto dto : customerList) {
                customerSelection.addItem(dto.getCustomerName());
            }
        }
    }

    private void populateSalesReps(UserDto[] salesRepList) {
        if (salesRepList.length == 0) {
            salesRepSelection.addItem("Select Sales Rep.");
        } else {
            salesRepSelection.removeAllItems();
            salesRepSelection.addItem("Select Sales Rep.");
            for (UserDto dto : salesRepList) {
                salesRepSelection.addItem(dto.getUserName());
            }
        }
    }

    private void addItemToSale() {
        if (itemDiscountTxt.getText() != null && qtyTxt.getText() != null && !qtyTxt.getText().equals("")) {
            if (Integer.parseInt(qtyTxt.getText()) > 0) {
                if (validateQty(Integer.parseInt(qtyTxt.getText()))) {
                    if (addItemToSalesItemList(selectedDto)) {
                        focusToTableCell(selectedDto);
                    } else {
                        addToSalesTable(selectedDto);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Product available quantity is not enough!", "Error", JOptionPane.ERROR_MESSAGE);
                }

            } else {
                JOptionPane.showMessageDialog(null, "Please add valid qty", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private boolean validateAddProduct() {
        boolean isValid = false;

        if (selectedDto != null) {
            isValid = true;
        }

        return isValid;
    }

    private boolean validateQty(int qty) {
        boolean isValid = false;
        if (selectedDto.getQuantity() >= qty) {
            System.out.println("selectedDto.getQuantity() = " + selectedDto.getQuantity());
            System.out.println("qty= " + qty);
            isValid = true;
        }

        return isValid;
    }

    private void addToSalesTable(ItemDto dto) {

        performCalculations(dto);
        System.out.println("addToSalesTable ------------------ --------------- --------------dto.getQuantity() = " + dto.getQuantity());
        DefaultTableModel model = (DefaultTableModel) salesTable.getModel();

        Object rowData[] = new Object[6];

        rowData[0] = dto.getItemName();
        rowData[1] = qtyTxt.getText();
        rowData[2] = dto.getUnitPrice();
        rowData[3] = totalPrice;
        rowData[4] = itemDiscountTxt.getText();
        rowData[5] = netSalesPrice;

        model.addRow(rowData);

        setSalesSummary();
        editQtyOfListItem(Integer.parseInt(qtyTxt.getText()), false);
        setNewQtyOfSalesItemMap(dto.getItemId(), Integer.parseInt(qtyTxt.getText()));
        System.out.println("qtyTxt.getText() = " + qtyTxt.getText());
    }

    private boolean addItemToSalesItemList(ItemDto itemDto) {
        boolean isAppear = false;
        System.out.println("32156156item id: " + itemDto.getItemId());

        ItemDto dtoInMap = salesItemMap.get(itemDto.getItemId());

        if (dtoInMap != null) {
            isAppear = true;
        } else {
            salesItemMap.put(itemDto.getItemId(), itemDto);
        }

        return isAppear;
    }

    private void focusToTableCell(ItemDto itemDto) {
        int row = 0;
        int col = 1;
        for (Integer id : salesItemMap.keySet()) {
            System.out.println("3333. Sales Map item id: " + id + " itemid " + itemDto.getItemId());
            if (Objects.equals(id, itemDto.getItemId())) {
                JOptionPane.showMessageDialog(null, "Already added the item update the qty");
                salesTable.setCellSelectionEnabled(true);
                salesTable.changeSelection(row, col, false, false);
                salesTable.editCellAt(row, col);
                salesTable.getEditorComponent().requestFocus();
                break;
            }
            row++;
        }
    }

    private void performCalculations(ItemDto item) {
        int qty = Integer.parseInt(qtyTxt.getText());
        BigDecimal unitPrice = item.getUnitPrice();
        BigDecimal discount;
        if (itemDiscountTxt.getText() != null && !itemDiscountTxt.getText().equals("")) {
            discount = new BigDecimal(Double.valueOf(itemDiscountTxt.getText()));
        } else {
            discount = new BigDecimal(BigInteger.ZERO);
        }

        totalPrice = unitPrice.multiply(new BigDecimal(qty));
        netSalesPrice = totalPrice.subtract(discount);
    }

    private void flushTable() {
        DefaultTableModel dtm = (DefaultTableModel) salesTable.getModel();
        dtm.setNumRows(0);
    }

    private void populateProductDetails(ItemDto itemDto) {
        System.out.println("Item Name: " + itemDto.getItemName());
        itemNameTxt.setText(itemDto.getItemName());
        itemPrice.setText(itemDto.getUnitPrice().toString());
        qtyTxt.requestFocus(true);
    }

    private void editQtyOfListItem(int qty, boolean qtyAdd) {

        int oldQty = selectedDto.getQuantity();
        int newQty;
        if (qtyAdd) {
            newQty = oldQty + qty;
        } else {
            newQty = oldQty - qty;
        }

        System.out.println("selectedDto.getItemName() = " + selectedDto.getItemName());
        System.out.println("qty = " + qty);
        System.out.println("oldQty = " + oldQty);
        System.out.println("newQty = " + newQty);

        selectedDto.setQuantity(newQty);

//        itemList.[selectedIndex] = new ItemDto(selectedDto);
        itemList.set(selectedIndex, new ItemDto(selectedDto));
//        itemList[selectedIndex] = selectedDto;
        System.out.println("editItemList===>selectedDtoAddress = " + new ItemDto(selectedDto));
        populateProducts(itemList);
    }

    private void performCalculationsOnCellUpdate(int qty, BigDecimal unitPrice, BigDecimal discount) {
        totalPrice = unitPrice.multiply(new BigDecimal(qty));
        netSalesPrice = totalPrice.subtract(discount);
    }

    private void updateSalesValuesInTable(int row) {
        salesTable.setValueAt(totalPrice, row, 3);
        salesTable.setValueAt(netSalesPrice, row, 5);
    }

    private Object[] getSelectedRowData(int row) {
        int colCount = salesTable.getColumnCount();
        System.out.println("col count: " + colCount);
        Object[] returnArr = new Object[6];

        for (int i = 0; i < returnArr.length; ++i) {
            System.out.println("val: " + salesTable.getValueAt(row, i));
            returnArr[i] = salesTable.getValueAt(row, i);
        }
        System.out.println("val at last col: " + salesTable.getValueAt(row, 5));

        return returnArr;
    }

    private void setSalesSummary() {
        BigDecimal totalSalesVal = new BigDecimal(BigInteger.ZERO);
        int totalQtySum = 0;

        for (int i = 0; i < salesTable.getRowCount(); i++) {
            totalQtySum += Integer.parseInt(String.valueOf(salesTable.getValueAt(i, 1)));
            totalSalesVal = totalSalesVal.add(new BigDecimal(String.valueOf(salesTable.getValueAt(i, 5))));
        }

        totalQtyTxt.setText(String.valueOf(totalQtySum));
        totalSalesValueTxt.setText(String.valueOf(totalSalesVal));
        balanceAmtTxt.setText(String.valueOf(totalSalesVal));

        totalQty = totalQtySum;
        totalSalesValue = totalSalesVal;
    }

    private boolean validateTotalDiscountReq() {
        boolean isValid = false;
        System.out.println("Slaes item map count: " + salesItemMap.size());
        System.out.println("totalQty: " + totalQty);

        if (salesItemMap.size() > 0 && validateBigDecimalAmounts(String.valueOf(totalDiscountTxt.getText()))) {
            isValid = true;
        }

        return isValid;
    }

    private void totalDiscountProcess() {
        if (String.valueOf(discountModeSelection.getSelectedItem()).equals("%")) {
            discountAmount = new BigDecimal(String.valueOf(totalDiscountTxt.getText()));
            totalBalance = totalSalesValue.subtract(totalSalesValue.multiply(discountAmount).divide(new BigDecimal(100)));
        } else {
            discountAmount = new BigDecimal(String.valueOf(totalDiscountTxt.getText()));
            totalBalance = totalSalesValue.subtract(discountAmount);
        }
        balanceAmtTxt.setText(totalBalance.toString());
        cashAmtTxt.requestFocus();
    }

    private boolean validateCashAmtTxtEntered() {
        boolean isValid = false;
        BigDecimal cashAmt = new BigDecimal(String.valueOf(cashAmtTxt.getText()));
        BigDecimal balanceL = new BigDecimal(String.valueOf(balanceAmtTxt.getText()));

        if (cashAmt.compareTo(new BigDecimal(BigInteger.ZERO)) == 1) {
            if (cashAmt.compareTo(balanceL) != 1) {
                isValid = true;
            }
        }

        return isValid;
    }

    private void completeSalesAction() {
        productItemList.setEnabled(true);
        itemDiscountTxt.setEnabled(false);
        salesTable.setEnabled(false);
        totalDiscountTxt.setEnabled(false);
        // qtyTxt.setEnabled(false);
    }

    private void newSalesAction() {
        productItemList.setEnabled(true);
        itemDiscountTxt.setEnabled(true);
        salesTable.setEnabled(true);
        totalDiscountTxt.setEnabled(true);
        qtyTxt.setEnabled(true);
    }

    private boolean validateBigDecimalAmounts(String amt) {
        boolean isValid = true;

        try {
            BigDecimal casted = new BigDecimal(amt);
        } catch (NumberFormatException e) {
            isValid = false;
        }

        return isValid;
    }

    private void processCashAmountTxtEntered() {
        cashAmount = new BigDecimal(String.valueOf(cashAmtTxt.getText()));
        creditAmount = totalBalance.subtract(cashAmount);
        creditAmtTxt.setText(creditAmount.toString());

        if (creditAmount.compareTo(new BigDecimal(BigInteger.ZERO)) == 0) {
            saveBtn.requestFocus();
            chequeAmount = new BigDecimal(BigInteger.ZERO);
        } else {
            addChequeBtn.requestFocus();
        }
    }

    private List<ItemDto> getItemList() {
        List<ItemDto> productLines = new ArrayList<>();
        try {
            salesItemMap.keySet().forEach((key) -> {
                productLines.add(salesItemMap.get(key));
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productLines;
    }

    private CustomerDto getSelectedCustomer() {
        CustomerDto selectedCusDto = null;
        try {
            int selectedCusIndex = customerSelection.getSelectedIndex();
            selectedCusDto = customerList[selectedCusIndex];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return selectedCusDto;
    }

    private InvoiceDto getInvoiceData() {
        String paymentType = getPaymentType();
        String invoiceType = getInvoiceType();

        InvoiceDto invoiceDto = new InvoiceDto();
        invoiceDto.setInvoiceNumber(invoiceNumber);
        invoiceDto.setInvoiceType(invoiceType);
        invoiceDto.setPaymentType(paymentType);
        invoiceDto.setUserName(loggedInUser.getUserName());
        invoiceDto.setInvoiceTotal(totalSalesValue);
        invoiceDto.setBalance(creditAmount);
        invoiceDto.setInvoiceBalance(creditAmount);
        invoiceDto.setPaidAmount(cashAmount);

        return invoiceDto;
    }

    private String getPaymentType() {
        String type;
        if (cashAmount.compareTo(new BigDecimal(BigInteger.ZERO)) == 0 && chequeAmount.compareTo(new BigDecimal(BigInteger.ZERO)) == 0) {
            type = null;
        } else if (cashAmount.compareTo(new BigDecimal(BigInteger.ZERO)) == 0) {
            type = StringTag.SALES_INVOICE_PAYMENT_TYPE_CHEQUE;
        } else if (chequeAmount.compareTo(new BigDecimal(BigInteger.ZERO)) == 0) {
            type = StringTag.SALES_INVOICE_PAYMENT_TYPE_CASH;
        } else {
            type = StringTag.SALES_INVOICE_PAYMENT_TYPE_BOTH;
        }

        return type;
    }

    private String getInvoiceType() {
        String type;
        if (creditAmount.compareTo(new BigDecimal(BigInteger.ZERO)) == 0) {
            type = StringTag.SALES_INVOICE_TYPE_DEBIT;
        } else {
            type = StringTag.SALES_INVOICE_TYPE_CREDIT;
        }

        return type;
    }

    private CashBookDto getCashBookDto() {
        CashBookDto cashBookDto = new CashBookDto();
        try {
            cashBookDto.setCashDate(new Date());
            cashBookDto.setDebit(cashAmount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cashBookDto;
    }

    private BankBookDto getBankBookDto() {
        BankBookDto bankBookDto = new BankBookDto();
        try {
            bankBookDto.setBank(bankName);
            bankBookDto.setBankDate(new Date());
            bankBookDto.setBankDescription("Test");
            bankBookDto.setChequeNumber(Integer.valueOf(chequeNumber));
            bankBookDto.setChequeType("Cash");
            bankBookDto.setDeposit(bankAmount);
            bankBookDto.setWithdrawal(new BigDecimal(BigInteger.ZERO));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bankBookDto;
    }

    private SaleDto getSaleDto() {
        SaleDto saleDto = new SaleDto();
        List<ItemDto> items = getItemList();
        CustomerDto customerDto = getSelectedCustomer();
        InvoiceDto invoiceDto = getInvoiceData();
        String paymentType = getPaymentType();
        String invoiceType = getInvoiceType();
        String description = "Sale made for customer " + customerDto.getCustomerName();

        CashBookDto cashBookDto;
        BankBookDto bankBookDto;

        if (paymentType.equals(StringTag.SALES_INVOICE_PAYMENT_TYPE_CASH)) {
            cashBookDto = getCashBookDto();
            cashBookDto.setCashBookDescription(description);
            saleDto.setCashBookDto(cashBookDto);
        } else if (paymentType.equals(StringTag.SALES_INVOICE_PAYMENT_TYPE_CHEQUE)) {
            bankBookDto = getBankBookDto();
            bankBookDto.setBankDescription(description);
            saleDto.setBankBookDto(bankBookDto);
        }

        saleDto.setItemDtos(items);
        saleDto.setCustomerId(customerDto.getCustomerId());
        saleDto.setDiscount(discountAmount);
        saleDto.setSalesDate(new Date());
        saleDto.setSalesNo(invoiceNumber);
        saleDto.setSalesQty(totalQty);
        saleDto.setSalesTime(new Timestamp(System.currentTimeMillis()));
        saleDto.setSalesType(invoiceType);
        saleDto.setUserName(selectedSalesRepName);
        saleDto.setInvoiceDto(invoiceDto);

        return saleDto;
    }

    private void processSalesAddRequest(SaleDto[] salesList) {
        try {
            if (salesList.length != 0) {
                boolean saleSuccess = true;
                for (SaleDto saleDto : salesList) {
                    if (saleDto.getResult().equals(StringTag.SALES_SAVE_RESULT_INSUFFICIENT_STOCK)) {
                        saleSuccess = false;
                    }
                }
                performSaleComplete(saleSuccess);

            } else {
                JOptionPane.showMessageDialog(null, "Error occured please restart the app or contact support", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error occured please restart the app or contact support", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void setNewQtyOfSalesItemMap(int selectedId, int qty) {
        salesItemMap.keySet().stream().filter((id) -> (selectedId == id)).forEachOrdered((id) -> {
//            int oldQty = salesItemMap.get(id).getQuantity();
            ItemDto itemDto = new ItemDto(salesItemMap.get(id));
            itemDto.setQuantity(qty);
            salesItemMap.replace(selectedId, itemDto);
        });
    }

    private void initialDisableFields() {
        itemDiscountTxt.setEnabled(false);
        customerSelection.setEnabled(false);
        qtyTxt.setEnabled(false);
        addProductBtn.setEnabled(false);
        salesTable.setEnabled(false);
        totalDiscountTxt.setEnabled(false);
        cashAmtTxt.setEnabled(false);
        addChequeBtn.setEnabled(false);
        saveBtn.setEnabled(false);
    }

    private void initialStartUpAddSale() {
//        salesRepSelection.setEnabled(false);
        itemDiscountTxt.setEnabled(true);
        customerSelection.setEnabled(true);
        qtyTxt.setEnabled(true);
        addProductBtn.setEnabled(true);
        salesTable.setEnabled(true);
        totalDiscountTxt.setEnabled(true);
        cashAmtTxt.setEnabled(true);
        addChequeBtn.setEnabled(true);
        saveBtn.setEnabled(true);
    }

    private void initialStartUpClear(boolean isSameAgent) {
        itemDiscountTxt.setText("0");
        customerSelection.setSelectedIndex(0);
        flushTable();
        if (!isSameAgent) {
            model.clear();
        }
        totalQtyTxt.setText("0");
        totalSalesValueTxt.setText(new BigDecimal(BigInteger.ZERO).toString());
        totalDiscountTxt.setText("0");
        balanceAmtTxt.setText(new BigDecimal(BigInteger.ZERO).toString());
        cashAmtTxt.setText(new BigDecimal(BigInteger.ZERO).toString());
        itemNameTxt.setText("name");
        itemPrice.setText("N/A");
    }

    private void performSaleComplete(boolean result) {
        if (result) {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "Your sale recorded, want to do new sale for same agent?", "Warning", dialogButton);

            if (dialogResult == JOptionPane.YES_OPTION) {
                prepForNewSale(true);
            } else {
                prepForNewSale(false);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Sale Failed", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void prepForNewSale(boolean isSameAgent) {
        if (!isSameAgent) {
            initialDisableFields();
        }
        initialStartUpClear(isSameAgent);
        salesRepSelection.setEnabled(true);
        salesRepSelection.requestFocus();
    }

    private void itemAddFinishConfirmation() {
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(null, "Want to finish item add?", "Warning", dialogButton);

        if (dialogResult == JOptionPane.YES_OPTION) {
            totalDiscountTxt.requestFocus();
        } else {
            productItemList.setSelectedIndex(selectedIndex);
        }
    }

    private void performSaleSave() {
        try {
            SaleDto saleDto = getSaleDto();
            SalesApiResponseHandler handler = new SalesApiResponseHandler();

            SaleDto[] addSaleData = handler.addSaleData(saleDto);
            processSalesAddRequest(addSaleData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        productItemList = new javax.swing.JList<>();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        salesRepSelection = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        invoiceNumberTxt = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        customerSelection = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        itemNameTxt = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        itemPrice = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        itemDiscountTxt = new javax.swing.JTextField();
        qtyTxt = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        addProductBtn = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        salesTable = new javax.swing.JTable();
        jLabel12 = new javax.swing.JLabel();
        totalQtyTxt = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        totalSalesValueTxt = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        totalDiscountTxt = new javax.swing.JTextField();
        discountModeSelection = new javax.swing.JComboBox<>();
        jLabel17 = new javax.swing.JLabel();
        balanceAmtTxt = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        cashAmtTxt = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        chequeAmtTxt = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        creditAmtTxt = new javax.swing.JTextField();
        addChequeBtn = new javax.swing.JButton();
        cancelBtn = new javax.swing.JButton();
        saveBtn = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(1246, 720));
        setLayout(new java.awt.BorderLayout());

        jPanel2.setBackground(new java.awt.Color(204, 204, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(212, 720));

        productItemList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                productItemListMouseClicked(evt);
            }
        });
        productItemList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                productItemListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(productItemList);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 102, 255));
        jLabel1.setText("PRODUCTS");

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 102, 255));
        jLabel2.setText("Prod Name |");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 102, 255));
        jLabel3.setText("Avail Qty");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 619, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel5.setPreferredSize(new java.awt.Dimension(1028, 729));
        jPanel5.setLayout(new java.awt.BorderLayout());

        jPanel6.setBackground(new java.awt.Color(204, 204, 255));
        jPanel6.setPreferredSize(new java.awt.Dimension(1028, 729));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 102, 255));
        jLabel4.setText("SALES");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addContainerGap(884, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(204, 204, 255));

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Sales Rep.");

        salesRepSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesRepSelectionActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setText("Invoice Number");

        invoiceNumberTxt.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        invoiceNumberTxt.setForeground(new java.awt.Color(255, 0, 0));
        invoiceNumberTxt.setText("inv");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Customer");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setText("Item Name");

        itemNameTxt.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        itemNameTxt.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        itemNameTxt.setText("Name");
        itemNameTxt.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        itemNameTxt.setMaximumSize(new java.awt.Dimension(100, 13));
        itemNameTxt.setMinimumSize(new java.awt.Dimension(50, 13));
        itemNameTxt.setPreferredSize(new java.awt.Dimension(70, 13));

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setText("Item Unit Price");

        itemPrice.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        itemPrice.setForeground(new java.awt.Color(255, 0, 0));
        itemPrice.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        itemPrice.setText("100");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("Item Discount");

        itemDiscountTxt.setText("0.00");

        qtyTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                qtyTxtActionPerformed(evt);
            }
        });
        qtyTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                qtyTxtKeyPressed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel18.setText("Quantity");

        addProductBtn.setBackground(new java.awt.Color(0, 102, 204));
        addProductBtn.setForeground(new java.awt.Color(255, 255, 255));
        addProductBtn.setText("Add Product");
        addProductBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addProductBtnActionPerformed(evt);
            }
        });

        salesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Name", "Product Quantity", "Unit Price", "Total Price", "Discount", "Net Sales Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        salesTable.getTableHeader().setReorderingAllowed(false);
        salesTable.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                salesTableKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(salesTable);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setText("Total Quantity");

        totalQtyTxt.setEditable(false);
        totalQtyTxt.setForeground(new java.awt.Color(255, 0, 0));
        totalQtyTxt.setText("0");
        totalQtyTxt.setEnabled(false);
        totalQtyTxt.setFocusable(false);
        totalQtyTxt.setRequestFocusEnabled(false);
        totalQtyTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totalQtyTxtActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setText("Total Sales Value");

        totalSalesValueTxt.setEditable(false);
        totalSalesValueTxt.setForeground(new java.awt.Color(255, 0, 0));
        totalSalesValueTxt.setText("0.00");
        totalSalesValueTxt.setEnabled(false);
        totalSalesValueTxt.setFocusable(false);
        totalSalesValueTxt.setRequestFocusEnabled(false);
        totalSalesValueTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totalSalesValueTxtActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel15.setText("Total Discount");

        totalDiscountTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                totalDiscountTxtKeyPressed(evt);
            }
        });

        discountModeSelection.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "%", "Rs" }));
        discountModeSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                discountModeSelectionActionPerformed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel17.setText("Balance");

        balanceAmtTxt.setEnabled(false);

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel19.setText("Cash Amount");

        cashAmtTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cashAmtTxtActionPerformed(evt);
            }
        });
        cashAmtTxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cashAmtTxtKeyPressed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel20.setText("Cheque Amount");

        chequeAmtTxt.setEditable(false);
        chequeAmtTxt.setForeground(new java.awt.Color(255, 0, 0));
        chequeAmtTxt.setText("0");
        chequeAmtTxt.setEnabled(false);
        chequeAmtTxt.setFocusable(false);
        chequeAmtTxt.setRequestFocusEnabled(false);
        chequeAmtTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chequeAmtTxtActionPerformed(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel22.setText("Credit Amount");

        creditAmtTxt.setEditable(false);
        creditAmtTxt.setForeground(new java.awt.Color(255, 0, 0));
        creditAmtTxt.setText("0");
        creditAmtTxt.setEnabled(false);
        creditAmtTxt.setFocusable(false);
        creditAmtTxt.setRequestFocusEnabled(false);
        creditAmtTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                creditAmtTxtActionPerformed(evt);
            }
        });

        addChequeBtn.setBackground(new java.awt.Color(0, 102, 204));
        addChequeBtn.setForeground(new java.awt.Color(255, 255, 255));
        addChequeBtn.setText("Add Cheque");
        addChequeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addChequeBtnActionPerformed(evt);
            }
        });

        cancelBtn.setBackground(new java.awt.Color(255, 0, 0));
        cancelBtn.setText("Cancel");

        saveBtn.setBackground(new java.awt.Color(51, 204, 0));
        saveBtn.setText("Save");
        saveBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveBtnMouseClicked(evt);
            }
        });
        saveBtn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                saveBtnKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(53, 53, 53)
                                .addComponent(salesRepSelection, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(55, 55, 55)
                                .addComponent(customerSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(43, 43, 43)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(24, 24, 24)
                                .addComponent(itemPrice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(50, 50, 50)
                                .addComponent(itemNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14))
                        .addGap(36, 36, 36)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(invoiceNumberTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(itemDiscountTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                .addComponent(qtyTxt))))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addGap(72, 72, 72)
                                .addComponent(balanceAmtTxt))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addGap(31, 31, 31)
                                .addComponent(totalQtyTxt))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addGap(18, 18, 18)
                                .addComponent(totalSalesValueTxt))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel8Layout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addGap(30, 30, 30)
                                .addComponent(totalDiscountTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(discountModeSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19)
                            .addComponent(jLabel20)
                            .addComponent(jLabel22))
                        .addGap(64, 64, 64)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(creditAmtTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                                .addComponent(cashAmtTxt))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addComponent(chequeAmtTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(addChequeBtn)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(addProductBtn, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                                .addComponent(cancelBtn)
                                .addGap(15, 15, 15)
                                .addComponent(saveBtn)))))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(invoiceNumberTxt))
                .addGap(20, 20, 20)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(itemPrice)
                        .addComponent(jLabel18)
                        .addComponent(qtyTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(salesRepSelection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabel11))
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(itemNameTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel16)
                                .addComponent(itemDiscountTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(17, 17, 17)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(customerSelection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))))
                .addGap(15, 15, 15)
                .addComponent(addProductBtn)
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(totalQtyTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addComponent(totalSalesValueTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(totalDiscountTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(discountModeSelection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(balanceAmtTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(cashAmtTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(chequeAmtTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addChequeBtn)
                            .addComponent(jLabel20))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(creditAmtTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel22))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cancelBtn)
                    .addComponent(saveBtn))
                .addGap(43, 43, 43))
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel5.add(jPanel6, java.awt.BorderLayout.WEST);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 729, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void qtyTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_qtyTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_qtyTxtActionPerformed

    private void qtyTxtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_qtyTxtKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (validateAddProduct()) {
                addItemToSale();
            } else {
                JOptionPane.showMessageDialog(null, "Please select product", "Error", JOptionPane.ERROR_MESSAGE);
            }

        }
        if (evt.getKeyCode() == KeyEvent.VK_F6) {
            itemAddFinishConfirmation();
        }
    }//GEN-LAST:event_qtyTxtKeyPressed

    private void addProductBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addProductBtnActionPerformed
        if (validateAddProduct()) {
            addItemToSale();
            if (evt.getActionCommand().equals(String.valueOf(KeyEvent.VK_ENTER))) {
                System.out.println("addProductBtnActionPerformed");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Please select product", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_addProductBtnActionPerformed

    private void salesTableKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_salesTableKeyPressed

    }//GEN-LAST:event_salesTableKeyPressed

    private void totalQtyTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totalQtyTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_totalQtyTxtActionPerformed

    private void totalSalesValueTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totalSalesValueTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_totalSalesValueTxtActionPerformed

    private void discountModeSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_discountModeSelectionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_discountModeSelectionActionPerformed

    private void chequeAmtTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chequeAmtTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_chequeAmtTxtActionPerformed

    private void creditAmtTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_creditAmtTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_creditAmtTxtActionPerformed

    private void addChequeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addChequeBtnActionPerformed

        try {
            AddChequeUi addCheque = new AddChequeUi(this);
            addCheque.setResizable(false);
            addCheque.setVisible(true);
            addCheque.setLocationRelativeTo(null); //Center the modal
            addCheque.setAlwaysOnTop(true);
            ChequeDataDto dto = (ChequeDataDto) addCheque.chequeDataDto;
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }//GEN-LAST:event_addChequeBtnActionPerformed

    private void cashAmtTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cashAmtTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cashAmtTxtActionPerformed

    private void productItemListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_productItemListMouseClicked
        try {
            selectedIndex = productItemList.getSelectedIndex();
            System.out.println("size= ++++++++++ " + itemList.size());
            System.out.println("selectedIndex = -------- " + selectedIndex);
            System.out.println("itemList.get(selectedIndex) = -------- " + itemList.get(selectedIndex));
            selectedDto = itemList.get(selectedIndex);
            System.out.println("productItemListMouseClicked===>selectedDtoAddress = " + selectedDto);
            populateProductDetails(selectedDto);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }//GEN-LAST:event_productItemListMouseClicked

    private void productItemListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_productItemListValueChanged

    }//GEN-LAST:event_productItemListValueChanged

    private void totalDiscountTxtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_totalDiscountTxtKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "Do you want to complete the sale?", "Warning", dialogButton);

            if (dialogResult == JOptionPane.YES_OPTION) {
                if (validateTotalDiscountReq()) {
                    totalDiscountProcess();
                    completeSalesAction();
                } else {
                    JOptionPane.showMessageDialog(null, "Please add products to proceed/discount amount not valid", "Error", JOptionPane.ERROR_MESSAGE);
                    totalDiscountTxt.setText(new BigDecimal(BigInteger.ZERO).toString());
                }
            } else {
                productItemList.requestFocus();
            }
        }
    }//GEN-LAST:event_totalDiscountTxtKeyPressed

    private void cashAmtTxtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cashAmtTxtKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (validateBigDecimalAmounts(String.valueOf(cashAmtTxt.getText()))) {
                if (validateCashAmtTxtEntered()) {
                    processCashAmountTxtEntered();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please add valid amount!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_cashAmtTxtKeyPressed

    private void saveBtnKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_saveBtnKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F10) {
            performSaleSave();
            qtyTxt.setEnabled(true);
            itemDiscountTxt.setEnabled(true);
            customerSelection.setEnabled(true);
        }
    }//GEN-LAST:event_saveBtnKeyPressed

    private void salesRepSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesRepSelectionActionPerformed
        try {
            int selectedRepIndex = salesRepSelection.getSelectedIndex();
            System.out.println("selectedRepIndex = " + selectedRepIndex);
            if (selectedRepIndex != 0) {
                UserDto selectedAgentDto = salesRepList[selectedRepIndex - 1];
                selectedSalesRepName = selectedAgentDto.getUserName();
                loadAgentItems(selectedAgentDto.getUserId(), new Date());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_salesRepSelectionActionPerformed

    private void saveBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveBtnMouseClicked
        performSaleSave();
    }//GEN-LAST:event_saveBtnMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addChequeBtn;
    private javax.swing.JButton addProductBtn;
    private javax.swing.JTextField balanceAmtTxt;
    private javax.swing.JButton cancelBtn;
    private javax.swing.JTextField cashAmtTxt;
    private javax.swing.JTextField chequeAmtTxt;
    private javax.swing.JTextField creditAmtTxt;
    private javax.swing.JComboBox<String> customerSelection;
    private javax.swing.JComboBox<String> discountModeSelection;
    private javax.swing.JLabel invoiceNumberTxt;
    private javax.swing.JTextField itemDiscountTxt;
    private javax.swing.JLabel itemNameTxt;
    private javax.swing.JLabel itemPrice;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JList<ProductItem> productItemList;
    private javax.swing.JTextField qtyTxt;
    private javax.swing.JComboBox<String> salesRepSelection;
    private javax.swing.JTable salesTable;
    private javax.swing.JButton saveBtn;
    private javax.swing.JTextField totalDiscountTxt;
    private javax.swing.JTextField totalQtyTxt;
    private javax.swing.JTextField totalSalesValueTxt;
    // End of variables declaration//GEN-END:variables

    @Override
    public void passChequeData(String bankName, Date chequeDate, String chequeNo, BigDecimal amount) {
        System.out.println("chequeNo = " + chequeNo + " ..bankName: " + bankName + " ..chequeDate: " + chequeDate.toString());
        chequeAmtTxt.setText(amount.toString());
        this.bankName = bankName;
        bankAmount = amount;
        chequeNumber = chequeNo;

        if (creditAmount.compareTo(amount) == -1) {
            JOptionPane.showMessageDialog(null, "Cheque amount is higher than the due amount", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            creditAmount = creditAmount.subtract(amount);
            creditAmtTxt.setText(creditAmount.toString());
            saveBtn.requestFocus();
        }
    }

//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(DashboardUiNew.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(DashboardUiNew.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(DashboardUiNew.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(DashboardUiNew.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new Sales().setVisible(true);
//            }
//        });
//    }
}
