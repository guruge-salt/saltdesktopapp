/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.ui;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Yachitha Sandaruwan
 */
public interface ChequeDataObserver {
    public void passChequeData(String bankName, Date chequeDate, String chequeNo, BigDecimal amount);
}
