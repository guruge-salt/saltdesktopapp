/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.ui;

import com.squareprolabs.apiresponse.UserApiResponseHandler;
import com.squareprolabs.dto.UserDto;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class DashboardUiNew extends javax.swing.JFrame {

    /**
     * Creates new form DashboardUiNew
     */
    
    CardLayout cardLayout;
    String userSelect;
    String username;
    ButtonGroup theme;
    UserDto userDto;
    
    
    public DashboardUiNew(UserDto userDto) {
        this.userDto = userDto;
        initComponents();
        showDateAndTime();
        navPanel.setVisible(true);
        menuPanel.setVisible(true);
        cardLayout = new CardLayout();
        userSelect = userDto.getUserName();
        this.username=userDto.getUserName();
        
        mainPanel.setLayout(cardLayout);
        mainPanel.add("First", new HomePage());
        mainPanel.add("Second", new Products(userDto.getUserName()));
        mainPanel.add("Third", new Customer(userDto));
        mainPanel.add("Seventh", new Logs());
        mainPanel.add("Eight", new ChangeDetails(userDto.getUserName()));
        mainPanel.add("Ninth", new CurrentStocks(userDto.getUserName()));
        mainPanel.add("Tenth", new SalesReport(userDto.getUserName()));
        mainPanel.add("Twelvth", new About());
        cardLayout.next(mainPanel);
        
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("ims-logo.png")));
        Toolkit theKit = getToolkit();
        Dimension dim = theKit.getScreenSize();
        setSize(dim);
        setTitle("Inventory Management System");
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    private void showDateAndTime() {
        try {
            final DateFormat dateformat1 = new SimpleDateFormat("yyyy-MM-dd");
            final DateFormat dateformat2 = new SimpleDateFormat("HH:mm:ss");
            int interval = 1000;

            new Timer(interval, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Calendar now = Calendar.getInstance();
                    date.setText(dateformat1.format(now.getTime()));
                    time.setText(dateformat2.format(now.getTime()));
                }
            }).start();

        } catch (Exception e) {
            e.getStackTrace();
        }
    }
    
    public void addProductsPage() {
        cardLayout.show(mainPanel, "Second");
    }

    public void addCustomersPage() {
        cardLayout.show(mainPanel, "Third");
    }

    public void addSuppliersPage() {
        Suppliers suppliers = new Suppliers(userDto);
        suppliers.loadTableData();
        mainPanel.add("Fourth", suppliers);
        cardLayout.show(mainPanel, "Fourth");
    }
    
    public void addUsersPage() {
        Users users = new Users();
        users.loadTableData();
        mainPanel.add("Sixth", users);
        cardLayout.show(mainPanel, "Sixth");
    }
    
    public void addLogsPage() {
        cardLayout.show(mainPanel, "Seventh");
    }
    
    public void addChangeDetailsPage() {
        cardLayout.show(mainPanel, "Eight");
    }
    
    public void addCurrentStocksPage() {
        cardLayout.show(mainPanel, "Ninth");
    }
    
    public void addSalesReportPage() {
        Sales sales = new Sales(userDto);
        mainPanel.add("Thirteenth", sales);
        cardLayout.show(mainPanel, "Thirteenth");
    }
    
    public void addMaterialPage() {
        Material material = new Material(userDto);
        mainPanel.add("Eleventh", material);
        cardLayout.show(mainPanel, "Eleventh");
    }
    
    public void addAboutPage() {
        cardLayout.show(mainPanel, "Twelvth");
    }
    
    public void addMaterialInPage() {
        MaterialIn materialIn = new MaterialIn(userDto);
        mainPanel.add("Thirteenth", materialIn);
        cardLayout.show(mainPanel, "Thirteenth");
    }
    
    public void addMaterialOutPage() {
        MaterialOut materialOut = new MaterialOut(userDto);
        mainPanel.add("Fourteenth", materialOut);
        cardLayout.show(mainPanel, "Fourteenth");
    }
    
    public void addAgentStockMangementPage() {
        AgentDailyStockManagement agentDailyStockManagement = new AgentDailyStockManagement();
        mainPanel.add("Fifteenth", agentDailyStockManagement);
        cardLayout.show(mainPanel, "Fifteenth");
    }
    
    public void addInvoiceListingPage() {
        InvoiceListing invoiceListing = new InvoiceListing();
        mainPanel.add("Sixteenth", invoiceListing);
        cardLayout.show(mainPanel, "Sixteenth");
    }
    
    public void addDailySalesReport() {
        DailySalesReport dailySalesReport = new DailySalesReport();
        mainPanel.add("Seventeenth", dailySalesReport);
        cardLayout.show(mainPanel, "Seventeenth");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        navPanel = new javax.swing.JPanel();
        suppliersBtn = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        productsBtn = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        materialBtn = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        customersBtn = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        currentStockBtn = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        salesBtn = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        usersBtn = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        menuPanel = new javax.swing.JPanel();
        menuBtn = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        date = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        time = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        homeMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();
        salesMenuItem = new javax.swing.JMenu();
        changeDetailsMenuItem = new javax.swing.JMenuItem();
        logoutMenuItem = new javax.swing.JMenuItem();
        materialMainMenu = new javax.swing.JMenu();
        materialInMenu = new javax.swing.JMenuItem();
        materialOutMenu = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        mainPanel.setPreferredSize(new java.awt.Dimension(1246, 720));

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1246, Short.MAX_VALUE)
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        navPanel.setPreferredSize(new java.awt.Dimension(114, 720));

        suppliersBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/supplierLarge.png"))); // NOI18N
        suppliersBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                suppliersBtnMouseClicked(evt);
            }
        });

        jLabel3.setText("Suppliers");

        productsBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/productLarge.png"))); // NOI18N
        productsBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                productsBtnMouseClicked(evt);
            }
        });

        jLabel5.setText("Products");

        materialBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/basket-icon.png"))); // NOI18N
        materialBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                materialBtnMouseClicked(evt);
            }
        });

        jLabel7.setText("Material");

        customersBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/customerLarge.png"))); // NOI18N
        customersBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                customersBtnMouseClicked(evt);
            }
        });

        jLabel9.setText("Customers");

        currentStockBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/inventory-maintenance-icon.png"))); // NOI18N
        currentStockBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                currentStockBtnMouseClicked(evt);
            }
        });

        jLabel11.setText("Current Stock");

        salesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/saleLarge.png"))); // NOI18N
        salesBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                salesBtnMouseClicked(evt);
            }
        });

        jLabel13.setText("Sales");

        usersBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/userLarge.png"))); // NOI18N
        usersBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                usersBtnMouseClicked(evt);
            }
        });

        jLabel15.setText("Users");

        javax.swing.GroupLayout navPanelLayout = new javax.swing.GroupLayout(navPanel);
        navPanel.setLayout(navPanelLayout);
        navPanelLayout.setHorizontalGroup(
            navPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(navPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(navPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(productsBtn)
                    .addComponent(jLabel9)
                    .addComponent(currentStockBtn)
                    .addComponent(jLabel11)
                    .addGroup(navPanelLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jLabel3))
                    .addComponent(customersBtn)
                    .addGroup(navPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel5)
                        .addComponent(materialBtn))
                    .addComponent(salesBtn)
                    .addComponent(usersBtn)
                    .addGroup(navPanelLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel15))
                    .addGroup(navPanelLayout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel13))
                    .addComponent(suppliersBtn)
                    .addComponent(jLabel7)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        navPanelLayout.setVerticalGroup(
            navPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(navPanelLayout.createSequentialGroup()
                .addComponent(suppliersBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(productsBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(materialBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(customersBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addGap(18, 18, 18)
                .addComponent(currentStockBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11)
                .addGap(18, 18, 18)
                .addComponent(salesBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13)
                .addGap(18, 18, 18)
                .addComponent(usersBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(111, Short.MAX_VALUE))
        );

        menuPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        menuBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/menu.png"))); // NOI18N
        menuBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                menuBtnMouseClicked(evt);
            }
        });
        menuPanel.add(menuBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 1, -1, 26));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("DATE: ");
        menuPanel.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1070, 0, -1, 20));

        date.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        date.setForeground(new java.awt.Color(102, 102, 255));
        date.setText("jLabel6");
        menuPanel.add(date, new org.netbeans.lib.awtextra.AbsoluteConstraints(1120, 0, -1, 20));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("TIME:");
        menuPanel.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(1230, 0, -1, 20));

        time.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        time.setForeground(new java.awt.Color(102, 102, 255));
        time.setText("jLabel7");
        menuPanel.add(time, new org.netbeans.lib.awtextra.AbsoluteConstraints(1270, 0, -1, 20));

        fileMenu.setText("File");

        homeMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.CTRL_MASK));
        homeMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/home.png"))); // NOI18N
        homeMenuItem.setText("Home");
        homeMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(homeMenuItem);

        aboutMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        aboutMenuItem.setText("About");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(aboutMenuItem);

        jMenuBar1.add(fileMenu);

        salesMenuItem.setText("Administration");

        changeDetailsMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_1, java.awt.event.InputEvent.CTRL_MASK));
        changeDetailsMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/changeDetails.png"))); // NOI18N
        changeDetailsMenuItem.setText("Change Details");
        changeDetailsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeDetailsMenuItemActionPerformed(evt);
            }
        });
        salesMenuItem.add(changeDetailsMenuItem);

        logoutMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_2, java.awt.event.InputEvent.CTRL_MASK));
        logoutMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/logoutSmall.png"))); // NOI18N
        logoutMenuItem.setText("Log Out");
        logoutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutMenuItemActionPerformed(evt);
            }
        });
        salesMenuItem.add(logoutMenuItem);

        materialMainMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/basket-icon small.png"))); // NOI18N
        materialMainMenu.setText("Material");

        materialInMenu.setText("MaterialIn");
        materialInMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                materialInMenuActionPerformed(evt);
            }
        });
        materialMainMenu.add(materialInMenu);

        materialOutMenu.setText("MaterialOut");
        materialOutMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                materialOutMenuActionPerformed(evt);
            }
        });
        materialMainMenu.add(materialOutMenu);

        salesMenuItem.add(materialMainMenu);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/productSmall.png"))); // NOI18N
        jMenu1.setText("Stock Management");

        jMenuItem1.setText("Agent Stock Management");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        salesMenuItem.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/squareprolabs/images/saleSmall.png"))); // NOI18N
        jMenu2.setText("Sales");

        jMenuItem2.setText("Invoice Listing");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuItem3.setText("Daily Sales Report");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        salesMenuItem.add(jMenu2);

        jMenuBar1.add(salesMenuItem);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(navPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(menuPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(menuPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(navPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void homeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeMenuItemActionPerformed
        DashboardUiNew dashboardUiNew = new DashboardUiNew(userDto);
        dispose();
    }//GEN-LAST:event_homeMenuItemActionPerformed

    private void changeDetailsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeDetailsMenuItemActionPerformed
        addChangeDetailsPage();
    }//GEN-LAST:event_changeDetailsMenuItemActionPerformed

    private void logoutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutMenuItemActionPerformed
        dispose();
        LoginUI ld = new LoginUI();
        ld.setLocationRelativeTo(null);
        ld.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ld.setVisible(true);
    }//GEN-LAST:event_logoutMenuItemActionPerformed

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        addAboutPage();
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void menuBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuBtnMouseClicked
        if(navPanel.isVisible()==false){
            navPanel.setVisible(true);
        }else{
            navPanel.setVisible(false);
        }
    }//GEN-LAST:event_menuBtnMouseClicked

    private void suppliersBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_suppliersBtnMouseClicked
        addSuppliersPage();
    }//GEN-LAST:event_suppliersBtnMouseClicked

    private void productsBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_productsBtnMouseClicked
        addProductsPage();
    }//GEN-LAST:event_productsBtnMouseClicked

    private void materialBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_materialBtnMouseClicked
        addMaterialPage();
    }//GEN-LAST:event_materialBtnMouseClicked

    private void customersBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_customersBtnMouseClicked
        addCustomersPage();
    }//GEN-LAST:event_customersBtnMouseClicked

    private void currentStockBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_currentStockBtnMouseClicked
        addCurrentStocksPage();
    }//GEN-LAST:event_currentStockBtnMouseClicked

    private void salesBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_salesBtnMouseClicked
        addSalesReportPage();
    }//GEN-LAST:event_salesBtnMouseClicked

    private void usersBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_usersBtnMouseClicked
        addUsersPage();
    }//GEN-LAST:event_usersBtnMouseClicked

    private void materialInMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_materialInMenuActionPerformed
        addMaterialInPage();
    }//GEN-LAST:event_materialInMenuActionPerformed

    private void materialOutMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_materialOutMenuActionPerformed
        addMaterialOutPage();
    }//GEN-LAST:event_materialOutMenuActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        addAgentStockMangementPage();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        addInvoiceListingPage();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        addDailySalesReport();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DashboardUiNew.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DashboardUiNew.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DashboardUiNew.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DashboardUiNew.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
////                new DashboardUiNew().setVisible(true);
//                new DashboardUiNew();
//            }
//        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem changeDetailsMenuItem;
    private javax.swing.JLabel currentStockBtn;
    private javax.swing.JLabel customersBtn;
    private javax.swing.JLabel date;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenuItem homeMenuItem;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JMenuItem logoutMenuItem;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JLabel materialBtn;
    private javax.swing.JMenuItem materialInMenu;
    private javax.swing.JMenu materialMainMenu;
    private javax.swing.JMenuItem materialOutMenu;
    private javax.swing.JLabel menuBtn;
    private javax.swing.JPanel menuPanel;
    private javax.swing.JPanel navPanel;
    private javax.swing.JLabel productsBtn;
    private javax.swing.JLabel salesBtn;
    private javax.swing.JMenu salesMenuItem;
    private javax.swing.JLabel suppliersBtn;
    private javax.swing.JLabel time;
    private javax.swing.JLabel usersBtn;
    // End of variables declaration//GEN-END:variables
}
