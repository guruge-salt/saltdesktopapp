/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.ui;

import com.squareprolabs.apiresponse.SalesApiResponseHandler;
import com.squareprolabs.apiresponse.UserApiResponseHandler;
import com.squareprolabs.dto.DailySalesReportDto;
import com.squareprolabs.dto.DailySalesReportItemDto;
import com.squareprolabs.dto.SaleAgentItemDto;
import com.squareprolabs.dto.UserDto;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class DailySalesReport extends javax.swing.JPanel {

    /**
     * Creates new form DailySalesReport
     */
    private SalesApiResponseHandler apiResponseHandler = new SalesApiResponseHandler();
    private ArrayList<UserDto> userList;
    private DefaultTableModel model;
    private UserDto[] salesRepData;
    private int totalSales = 0;
    private BigDecimal totalSalesAmount = new BigDecimal(BigInteger.ZERO);
    private int totalCreditSales = 0;
    private BigDecimal totalCreditSalesAmount = new BigDecimal(BigInteger.ZERO);

    public DailySalesReport() {
        initComponents();
        model = (DefaultTableModel) DailySalesTable.getModel();
        refreshDailySalesTable();
        loadData();
    }

    private void refreshDailySalesTable() {
        DefaultTableModel dtm = (DefaultTableModel) DailySalesTable.getModel();
        dtm.setRowCount(0);
    }

    private void loadData() {
        UserApiResponseHandler userApiResponseHandler = new UserApiResponseHandler();
        salesRepData = userApiResponseHandler.getSalesRepData();
        agentSelection.removeAll();
        agentSelection.addItem("ALL");

        for (UserDto userDto : salesRepData) {
            agentSelection.addItem(userDto.getUserName());
        }
    }

    private void loadDailySalesRepotData(int selectedIndex) {
        final DateFormat dateformat1 = new SimpleDateFormat("yyyy-MM-dd");
        SaleAgentItemDto dto = new SaleAgentItemDto();
        dto.setAgentStockDate(new Date());
        int t = selectedIndex == 0 ? -1 : selectedIndex;
        if (t != -1) {
            UserDto aDto = salesRepData[selectedIndex - 1];
            dto.setId(aDto.getUserId());
        } else {
            dto.setSearchValue(-1);
        }
        DailySalesReportDto dailySalesReport = apiResponseHandler.getDailySalesReport(dto);
        Map<String, DailySalesReportItemDto> dailySalesByAgentMap = dailySalesReport.getDailySalesByAgent();
        Object rowData[] = new Object[8];
        totalSales = 0;
        totalSalesAmount = new BigDecimal(BigInteger.ZERO);
        totalCreditSales = 0;
        totalCreditSalesAmount = new BigDecimal(BigInteger.ZERO);

        try {
            for (String invoiceNo : dailySalesByAgentMap.keySet()) {
                rowData[2] = invoiceNo;
                System.out.println("invoiceNo = " + invoiceNo);
                rowData[3] = dailySalesByAgentMap.get(invoiceNo).getSaleItems();
                System.out.println(dailySalesByAgentMap.get(invoiceNo).getSaleItems());
                rowData[0] = dailySalesByAgentMap.get(invoiceNo).getCustomerName();
                System.out.println(dailySalesByAgentMap.get(invoiceNo).getCustomerName());
                rowData[1] = dailySalesByAgentMap.get(invoiceNo).getSalesRefName();
                System.out.println(dailySalesByAgentMap.get(invoiceNo).getSalesRefName());
                try {
                    rowData[4] = dailySalesByAgentMap.get(invoiceNo).getCashBookDto().getBalance();
                } catch (Exception e) {
                    rowData[4] = 0;
                }

                System.out.println(dailySalesByAgentMap.get(invoiceNo).getCashBookDto().getBalance());

                BigDecimal invoiceTotal = dailySalesByAgentMap.get(invoiceNo).getInvoiceTotal();
                rowData[7] = dailySalesByAgentMap.get(invoiceNo).getInvoiceTotal();

                if (invoiceTotal != null && !invoiceTotal.equals(0)) {
                    totalSalesAmount = totalSalesAmount.add(invoiceTotal);
                }
                System.out.println("*********************" + dailySalesByAgentMap.get(invoiceNo).getInvoiceBalance());

                BigDecimal invoiceBalance = dailySalesByAgentMap.get(invoiceNo).getInvoiceBalance();

                rowData[6] = dailySalesByAgentMap.get(invoiceNo).getInvoiceBalance();

                if (invoiceBalance != null & invoiceBalance.compareTo(new BigDecimal(0)) == 1) {
                    System.out.println("invoiceBalance inside for = " + invoiceBalance);
                    totalCreditSales = totalCreditSales + 1;
                    totalCreditSalesAmount = totalCreditSalesAmount.add(invoiceBalance);
                }
                try {
                    rowData[5] = dailySalesByAgentMap.get(invoiceNo).getBankBookDto().getDeposit();
                } catch (Exception e) {
                    rowData[5] = 0;
                }

                System.out.println(dailySalesByAgentMap.get(invoiceNo).getInvoiceTotal());
                model.addRow(rowData);
                totalSales = model.getRowCount();
                totalSalesLabel.setText(String.valueOf(totalSales));
                totalCreditSalesLbl.setText(String.valueOf(totalCreditSales));
                totalSalesAmountLbl.setText(String.valueOf(totalSalesAmount));
                totalCreditSalesAmountLbl.setText(String.valueOf(totalCreditSalesAmount));
            }
        } catch (NullPointerException exception) {
            JOptionPane.showMessageDialog(this, "No Daily Sales Record Found");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        agentSelection = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        DailySalesTable = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        totalSalesLabel = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        totalSalesAmountLbl = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        totalCreditSalesLbl = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        totalCreditSalesAmountLbl = new javax.swing.JLabel();

        setLayout(new java.awt.BorderLayout());

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Comfortaa", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 204));
        jLabel1.setText("Daily Sales Report");
        jPanel3.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, -1, -1));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Select Sales Agent"));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Select Agent");

        agentSelection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agentSelectionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addGap(39, 39, 39)
                .addComponent(agentSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(agentSelection, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(41, Short.MAX_VALUE))
        );

        DailySalesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Customer No", "Name", "Invoice No", "Items", "Cash", "Cheque", "Credit", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(DailySalesTable);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 419, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 51, 51));
        jLabel7.setText("Total Sales ");

        totalSalesLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        totalSalesLabel.setForeground(new java.awt.Color(102, 153, 255));
        totalSalesLabel.setText("0");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 51, 51));
        jLabel9.setText("Total Sales Amout");

        totalSalesAmountLbl.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        totalSalesAmountLbl.setForeground(new java.awt.Color(102, 153, 255));
        totalSalesAmountLbl.setText("0");

        jButton2.setBackground(new java.awt.Color(0, 255, 0));
        jButton2.setText("Print");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 51, 51));
        jLabel11.setText("Total Credit Sales");

        totalCreditSalesLbl.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        totalCreditSalesLbl.setForeground(new java.awt.Color(102, 153, 255));
        totalCreditSalesLbl.setText("0");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 51, 51));
        jLabel13.setText("Total Credit Sales Amount");

        totalCreditSalesAmountLbl.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        totalCreditSalesAmountLbl.setForeground(new java.awt.Color(102, 153, 255));
        totalCreditSalesAmountLbl.setText("0");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(totalSalesLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(26, 26, 26)
                        .addComponent(totalCreditSalesLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(totalSalesAmountLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(totalCreditSalesAmountLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(407, 407, 407)
                .addComponent(jButton2)
                .addGap(145, 145, 145))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(totalSalesLabel)
                    .addComponent(jLabel11)
                    .addComponent(totalCreditSalesLbl)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(totalSalesAmountLbl)
                    .addComponent(jLabel13)
                    .addComponent(totalCreditSalesAmountLbl))
                .addGap(20, 20, 20))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void agentSelectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agentSelectionActionPerformed

        refreshDailySalesTable();
        int selectedIndex = agentSelection.getSelectedIndex();
        loadDailySalesRepotData(selectedIndex);
    }//GEN-LAST:event_agentSelectionActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable DailySalesTable;
    private javax.swing.JComboBox<String> agentSelection;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel totalCreditSalesAmountLbl;
    private javax.swing.JLabel totalCreditSalesLbl;
    private javax.swing.JLabel totalSalesAmountLbl;
    private javax.swing.JLabel totalSalesLabel;
    // End of variables declaration//GEN-END:variables
}
