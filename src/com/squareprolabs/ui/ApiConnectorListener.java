/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.ui;

/**
 *
 * @author Yachitha Sandaruwan
 */
public interface ApiConnectorListener {
    public void startApiCall();
    public void endApiCall();
}
