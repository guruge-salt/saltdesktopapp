/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.apiresponse;

import com.google.gson.Gson;
import com.squareprolabs.apiconnector.ApiConnector;
import com.squareprolabs.dto.SupplierDto;
import com.squareprolabs.dto.UserDto;
import com.squareprolabs.ui.LoginUI;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class SupplierApiResponseHanlder {
    
    public SupplierDto[] getSupplierData(){
        try{
            String performRequest = ApiConnector.performRequest(null, "/supplier/getAllSuppliers", "GET");
            System.out.println("post = " + performRequest);
            SupplierDto[] supplierList = new Gson().fromJson(performRequest, SupplierDto[].class);
            
            for (SupplierDto supplierDto : supplierList) {
                System.out.println("supplierDto = " + supplierDto.getSupplierName());
            }
            
            return supplierList;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public SupplierDto addSupplier(SupplierDto supplierDto){
        try {
            String performRequest = ApiConnector.performRequest(supplierDto, "/supplier/saveSupplier", "POST");
            System.out.println("post = " + performRequest);
            SupplierDto returnDto = new Gson().fromJson(performRequest, SupplierDto.class);
            
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public SupplierDto updateSupplier(SupplierDto supplierDto){
        try {
            String performRequest = ApiConnector.performRequest(supplierDto, "/supplier/updateSupplier", "POST");
            System.out.println("post = " + performRequest);
            SupplierDto returnDto = new Gson().fromJson(performRequest, SupplierDto.class);
            
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public SupplierDto findUserByTpNumber(SupplierDto supplierDto) {
        try {
            String performRequest = ApiConnector.performRequest(supplierDto, "/supplier/deleteSupplier", "POST");
            System.out.println("post = " + performRequest);
            SupplierDto returnDto = new Gson().fromJson(performRequest, SupplierDto.class);
            
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public SupplierDto[] searchUserByTpNumber(SupplierDto supplierDto) {
        try {
            String performRequest = ApiConnector.performRequest(supplierDto, "/supplier/findSupplier", "POST");
            System.out.println("post = " + performRequest);
            SupplierDto[] supplierList = new Gson().fromJson(performRequest, SupplierDto[].class);
            
            return supplierList;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public SupplierDto[] findSupplierByNameOrTpNo(SupplierDto supplierDto) {
        try {
            String performRequest = ApiConnector.performRequest(supplierDto, "/supplier/findSupplierByNameOrTpNo", "POST");
            System.out.println("post = " + performRequest);
            SupplierDto[] supplierList = new Gson().fromJson(performRequest, SupplierDto[].class);
            
            return supplierList;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
