/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.apiresponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareprolabs.apiconnector.ApiConnector;
import com.squareprolabs.dto.ItemDto;
import com.squareprolabs.dto.MaterialDto;
import com.squareprolabs.dto.StockInDto;
import com.squareprolabs.ui.LoginUI;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class ProductApiResponseHandler {

    public ItemDto[] getItemData() {
        try {
            String performRequest = ApiConnector.performRequest(null, "/item/getAllItems", "GET");
            System.out.println("post = " + performRequest);
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
            ItemDto[] materialList = gson.fromJson(performRequest, ItemDto[].class);
            return materialList;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ItemDto[] addSupplier(ItemDto[] itemDtoList) {
        try {
            String performRequest = ApiConnector.performRequest(itemDtoList, "/stockIn/saveStockIn", "POST");
            System.out.println("post = " + performRequest);
            ItemDto[] returnDtoList = new Gson().fromJson(performRequest, ItemDto[].class);

            return returnDtoList;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ItemDto addProduct(ItemDto itemDto) {
        try {
            String performRequest = ApiConnector.performRequest(itemDto, "/item/addItem", "POST");
            ItemDto returnDto = new Gson().fromJson(performRequest, ItemDto.class);
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public ItemDto editProduct(ItemDto itemDto) {
        try {
            String performRequest = ApiConnector.performRequest(itemDto, "/item/updateItem", "POST");
            ItemDto returnDto = new Gson().fromJson(performRequest, ItemDto.class);
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
