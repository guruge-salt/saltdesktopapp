/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.apiresponse;

import com.google.gson.Gson;
import com.squareprolabs.apiconnector.ApiConnector;
import com.squareprolabs.dto.CustomerDto;
import com.squareprolabs.ui.LoginUI;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class CustomerApiHandler {
    
    public CustomerDto[] getCustomerData(){
        try{
            String performRequest = ApiConnector.performRequest(null, "/customer/getAllCustomer", "GET");
            System.out.println("post = " + performRequest);
            CustomerDto[] customerList = new Gson().fromJson(performRequest, CustomerDto[].class);
            
            return customerList;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public CustomerDto addCustomer(CustomerDto customerDto){
        try{
            String performRequest = ApiConnector.performRequest(customerDto, "/customer/addCustomer", "POST");
            System.out.println("post = " + performRequest);
            CustomerDto customerList = new Gson().fromJson(performRequest, CustomerDto.class);
            
            return customerList;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public CustomerDto updateCustomer(CustomerDto customerDto){
        try{
            String performRequest = ApiConnector.performRequest(customerDto, "/customer/updateCustomer", "POST");
            System.out.println("post = " + performRequest);
            CustomerDto customerList = new Gson().fromJson(performRequest, CustomerDto.class);
            
            return customerList;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public CustomerDto deleteCustomer(CustomerDto customerDto){
        try{
            String performRequest = ApiConnector.performRequest(customerDto, "/customer/deleteCustomer", "POST");
            System.out.println("post = " + performRequest);
            CustomerDto customerList = new Gson().fromJson(performRequest, CustomerDto.class);
            
            return customerList;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
