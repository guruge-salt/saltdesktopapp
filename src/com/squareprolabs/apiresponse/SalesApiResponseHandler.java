/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.apiresponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareprolabs.apiconnector.ApiConnector;
import com.squareprolabs.dto.DailySalesReportDto;
import com.squareprolabs.dto.DailySalesReportItemDto;
import com.squareprolabs.dto.ItemDto;
import com.squareprolabs.dto.MaterialDto;
import com.squareprolabs.dto.SaleAgentItemDto;
import com.squareprolabs.dto.SaleDto;
import com.squareprolabs.ui.DailySalesReport;
import com.squareprolabs.ui.LoginUI;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Yachitha Sandaruwan
 */
public class SalesApiResponseHandler {
    
    public static void main(String[] args) {
        SalesApiResponseHandler salesApiResponseHandler = new SalesApiResponseHandler();
        SaleAgentItemDto dto = new SaleAgentItemDto();
        dto.setAgentStockDate(new Date());
        dto.setId(2);
        dto.setSearchValue(-1);
        DailySalesReportDto dailySalesReport = salesApiResponseHandler.getDailySalesReport(dto);
        Map<String, DailySalesReportItemDto> dailySalesByAgent = dailySalesReport.getDailySalesByAgent();
        
//        dailySalesByAgent.keySet().forEach((key) -> {
//            dailySalesByAgent.get(key).stream().forEach(t->{
//                t.getSaleItems().stream().forEach(k->{
//                    System.out.println("k.getQuantity() = " + k.getQuantity());
//                });
//            });
//        });
    }
    
    public String getInvoiceNumber () {
        try{
            String id = "I";
            String str = new String(id);
            
            String performRequest = ApiConnector.performRequest(str, "/AutoIDGenerator/getID", "POST");
            System.out.println("inv num = " + performRequest);
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
            String invoiceNumber = gson.fromJson(performRequest, String.class);
            
            return invoiceNumber;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public SaleDto[] addSaleData(SaleDto saleDto) {
        System.out.println("############################## = " + "call sale add" + "############################");
        try {
            String performRequest = ApiConnector.performRequest(saleDto, "/sale/addSale", "POST");
            System.out.println("post = " + performRequest);
            SaleDto[] returnDto = new Gson().fromJson(performRequest, SaleDto[].class);
            
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public List<ItemDto> getAgentItems(SaleAgentItemDto dto) {
        
        try {
            String performRequest = ApiConnector.performRequest(dto, "/sale/getSalesQtyForAgent", "POST");
            System.out.println("post = " + performRequest);
            ItemDto[] returnDto = new Gson().fromJson(performRequest, ItemDto[].class);
            
            return Arrays.asList(returnDto);
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public DailySalesReportDto getDailySalesReport(SaleAgentItemDto dto) {
        
        try {
            String performRequest = ApiConnector.performRequest(dto, "/sale/getDailySalesReportItem", "POST");
            System.out.println("post = " + performRequest);
            DailySalesReportDto returnDto = new Gson().fromJson(performRequest, DailySalesReportDto.class);
            
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
