package com.squareprolabs.apiresponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareprolabs.apiconnector.ApiConnector;
import com.squareprolabs.dto.MaterialDto;
import com.squareprolabs.dto.MaterialInDto;
import com.squareprolabs.dto.MaterialOutDto;
import com.squareprolabs.dto.SupplierDto;
import com.squareprolabs.ui.ApiConnectorListener;
import com.squareprolabs.ui.LoginUI;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Yachitha Sandaruwan
 */
public class MaterialApiHandler {
    
    private ApiConnectorListener listener;
    
    public MaterialApiHandler(){
    }
    
    public MaterialApiHandler(ApiConnectorListener listener){
        this.listener = listener;
    }
    
    public MaterialDto[] getMaterialData(){
        try{
            String performRequest = ApiConnector.performRequest(null, "/material/getAllMaterial", "GET");
            System.out.println("post = " + performRequest);
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
//            listener.startApiCall();
            MaterialDto[] materialList = gson.fromJson(performRequest, MaterialDto[].class);
//            listener.endApiCall();
            
            for (MaterialDto supplierDto : materialList) {
                System.out.println("supplierDto = " + supplierDto.getSupplierName());
            }
            
            return materialList;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public SupplierDto[] getSupplierData(){
        try{
            String performRequest = ApiConnector.performRequest(null, "/supplier/getAllSuppliers", "GET");
            System.out.println("post = " + performRequest);
            SupplierDto[] supplierList = new Gson().fromJson(performRequest, SupplierDto[].class);
            
            for (SupplierDto supplierDto : supplierList) {
                System.out.println("supplierDto = " + supplierDto.getSupplierName());
            }
            
            return supplierList;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public MaterialDto addMaterial(MaterialDto materialDto) {
        try {
            String performRequest = ApiConnector.performRequest(materialDto, "/material/saveMaterial", "POST");
            System.out.println("post = " + performRequest);
            MaterialDto returnDto = new Gson().fromJson(performRequest, MaterialDto.class);
            
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public MaterialDto editMaterial(MaterialDto materialDto) {
        try {
            String performRequest = ApiConnector.performRequest(materialDto, "/material/updateMaterial", "POST");
            System.out.println("post = " + performRequest);
            MaterialDto returnDto = new Gson().fromJson(performRequest, MaterialDto.class);
            System.out.println(returnDto);
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public MaterialInDto[] getMaterialInData(){
        try{
            String performRequest = ApiConnector.performRequest(null, "/materialIn/getAllInMaterial", "GET");
            System.out.println("post = " + performRequest);
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
            MaterialInDto[] materialInList = gson.fromJson(performRequest, MaterialInDto[].class);
            
            
            return materialInList;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public MaterialInDto addMaterialInData(MaterialInDto materialInDto) {
        try {
            String performRequest = ApiConnector.performRequest(materialInDto, "/materialIn/saveMaterialIn", "POST");
            System.out.println("post = " + performRequest);
            MaterialInDto returnDto = new Gson().fromJson(performRequest, MaterialInDto.class);
            
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public MaterialOutDto[] getMaterialOutData(){
        try{
            String performRequest = ApiConnector.performRequest(null, "/MaterialOut/getAllOutMaterial", "GET");
            System.out.println("post = " + performRequest);
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
            MaterialOutDto[] materialOutList = gson.fromJson(performRequest, MaterialOutDto[].class);
            
            
            return materialOutList;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public MaterialOutDto addMaterialOutData(MaterialOutDto materialOutDto) {
        try {
            String performRequest = ApiConnector.performRequest(materialOutDto, "/MaterialOut/saveMaterialOut", "POST");
            System.out.println("post = " + performRequest);
            MaterialOutDto returnDto = new Gson().fromJson(performRequest, MaterialOutDto.class);
            
            return returnDto;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
