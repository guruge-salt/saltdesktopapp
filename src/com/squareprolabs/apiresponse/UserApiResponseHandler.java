/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.apiresponse;

import com.google.gson.Gson;
import com.squareprolabs.apiconnector.ApiConnector;
import com.squareprolabs.dto.UserDto;
import com.squareprolabs.ui.LoginUI;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class UserApiResponseHandler {
    
    public UserDto[] getUserData(){
        try{
//            ApiConnector.BASE_URL = "http://192.168.0.102:9090/api";
            String performRequest = ApiConnector.performRequest(null, "/user/getAllUsers", "GET");
            System.out.println("post = " + performRequest);
            UserDto[] userList = new Gson().fromJson(performRequest, UserDto[].class);
            
            return userList;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public UserDto[] getSalesRepData(){
        try{
            String performRequest = ApiConnector.performRequest(null, "/user/getAllSalesRefs", "GET");
            System.out.println("post = " + performRequest);
            UserDto[] userList = new Gson().fromJson(performRequest, UserDto[].class);
            
            return userList;
        } catch (IOException ex){
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
