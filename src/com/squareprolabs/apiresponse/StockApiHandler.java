/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.apiresponse;

import com.google.gson.Gson;
import com.squareprolabs.apiconnector.ApiConnector;
import com.squareprolabs.dto.StockInDto;
import com.squareprolabs.dto.SupplierDto;
import com.squareprolabs.ui.LoginUI;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class StockApiHandler {
    public StockInDto[] addSupplier(StockInDto[] stockInDtoList){
        try {
            String performRequest = ApiConnector.performRequest(stockInDtoList, "/stockIn/saveStockIn", "POST");
            System.out.println("post = " + performRequest);
            StockInDto[] returnDtoList = new Gson().fromJson(performRequest, StockInDto[].class);
            
            return returnDtoList;
        } catch (IOException ex) {
            Logger.getLogger(LoginUI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static void main(String[] args) {
        StockApiHandler apiHandler = new StockApiHandler();
        
        StockInDto[] dtos = new StockInDto[2];
        
        StockInDto dto1 = new StockInDto();
        dto1.setItemId(1);
        dto1.setQuantity(2);
        dto1.setSalesRepId(1);
        dto1.setStockDate(new Date());
        dto1.setStockIn(1);
        dto1.setStockInBalance(0);
//        dto1.setStockInNo("SIN001");
        dto1.setStockTime(new Timestamp(0));
        dto1.setStockType("Sell");
        dto1.setUserId(1);
        
        StockInDto dto2 = new StockInDto();
        dto2.setItemId(2);
        dto2.setQuantity(4);
        dto2.setSalesRepId(1);
        dto2.setStockDate(new Date());
        dto2.setStockIn(4);
        dto2.setStockInBalance(0);
//        dto2.setStockInNo("SIN002");
        dto2.setStockTime(new Timestamp(0));
        dto2.setStockType("Sell sell");
        dto2.setUserId(1);
        
        dtos[0] = dto1;
        dtos[1] = dto2;
        
        StockInDto[] returnD = apiHandler.addSupplier(dtos);
        
        System.out.println("Return Value: " + returnD);
    }
}
