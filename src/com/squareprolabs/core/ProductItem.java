/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.core;

import javax.swing.Icon;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class ProductItem {
    public String productName;
    public int availableQty;
    public Icon icon;

    public ProductItem(String productName, int availableQty, Icon icon) {
        this.productName = productName;
        this.availableQty = availableQty;
        this.icon = icon;
    }
}
