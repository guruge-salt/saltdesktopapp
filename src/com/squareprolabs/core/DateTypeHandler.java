/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.core;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class DateTypeHandler {
    
    private static DateTypeHandler dateTypeHandler;
    
    private DateTypeHandler(){
        
    }
    
    public static DateTypeHandler getDateTypeHandler() {
        
        if (dateTypeHandler == null) {
            dateTypeHandler = new DateTypeHandler();
        }
        
        return dateTypeHandler;
    }
    
    public Date getDate(Date inputDate){
        Date outputdate;
        
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        String formattedStr = simpleDateFormat.format(inputDate);
        
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(formattedStr, inputFormatter);

        outputdate = java.sql.Date.valueOf(localDate);
        
        return outputdate;
    } 
}
