/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.core;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class StringTag {
    // sales
    public static final String SALES_INVOICE_TYPE_DEBIT = "DEBIT";
    public static final String SALES_INVOICE_TYPE_CREDIT = "CREDIT";
    public static final String SALES_INVOICE_PAYMENT_TYPE_CASH = "CASH";
    public static final String SALES_INVOICE_PAYMENT_TYPE_CHEQUE = "CHEQUE";
    public static final String SALES_INVOICE_PAYMENT_TYPE_BOTH = "BOTH";
    public static final String SALES_INVOICE_STATUS_OPEN = "OPEN";
    public static final String SALES_INVOICE_STATUS_CLOSE = "CLOSE";
    public static final String SALES_SAVE_RESULT_INSUFFICIENT_STOCK = "insufficientStock";
    public static final String SALES_SAVE_RESULT_SALE_SAVED = "saleSaved";
    
    // customer
    public static final String CUSTOMER_STATUS_ACTIVE = "ACTIVE";
    public static final String CUSTOMER_STATUS_INACTIVE = "INACTIVE";
    public static final String CUSTOMER_TYPE_WHOLE_SALE = "WHOLE_SALE";
    public static final String CUSTOMER_TYPE_RETAIL = "RETAIL";
    public static final String CUSTOMER_ADD_SAVED_STATUS = "customerSaved";
    public static final String CUSTOMER_UPDATE_NOT_FOUND = "customerNotFound";
    public static final String CUSTOMER_DELETE_SUCCESS = "customerDeleted";
    
    
    // role
    public static final String USER_ROLE_TYPE_USER = "USER";
    public static final String USER_ROLE_TYPE_ADMIN = "ADMIN";
    public static final String USER_ROLE_TYPE_SUPER_ADMIN = "SUPER_ADMIN";
}
