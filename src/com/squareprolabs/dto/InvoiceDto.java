/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.dto;

import java.math.BigDecimal;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class InvoiceDto {
    private String userName;
    private String invoiceNumber;
    private String result;
    private String paymentType;
    private String invoiceType;
    private CashBookDto cashBookDto;
    private BankBookDto bankBookDto;
    private String invoiceStatus;
    private BigDecimal balance;
    private BigDecimal invoiceBalance;
    private BigDecimal invoiceTotal;
    private BigDecimal paidAmount;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public CashBookDto getCashBookDto() {
        return cashBookDto;
    }

    public void setCashBookDto(CashBookDto cashBookDto) {
        this.cashBookDto = cashBookDto;
    }

    public BankBookDto getBankBookDto() {
        return bankBookDto;
    }

    public void setBankBookDto(BankBookDto bankBookDto) {
        this.bankBookDto = bankBookDto;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getInvoiceBalance() {
        return invoiceBalance;
    }

    public void setInvoiceBalance(BigDecimal invoiceBalance) {
        this.invoiceBalance = invoiceBalance;
    }

    public BigDecimal getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(BigDecimal invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }
}
