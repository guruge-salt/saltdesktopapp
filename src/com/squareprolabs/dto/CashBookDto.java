/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class CashBookDto {
    private Date cashDate;
    private String cashBookDescription;
    private BigDecimal Debit;
    private BigDecimal credit;
    private BigDecimal balance;

    public Date getCashDate() {
        return cashDate;
    }

    public void setCashDate(Date cashDate) {
        this.cashDate = cashDate;
    }

    public String getCashBookDescription() {
        return cashBookDescription;
    }

    public void setCashBookDescription(String cashBookDescription) {
        this.cashBookDescription = cashBookDescription;
    }

    public BigDecimal getDebit() {
        return Debit;
    }

    public void setDebit(BigDecimal Debit) {
        this.Debit = Debit;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
    
    
}
