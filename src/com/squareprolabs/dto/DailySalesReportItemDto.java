package com.squareprolabs.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class DailySalesReportItemDto {
    private int saleId;
    private int searchValue;
    private String salesRefName;
    private String salesNo;
    private int customerId;
    private String customerName;
    private String saleItems;
    private String salesType;
    private BigDecimal invoiceTotal;
    private BigDecimal invoiceBalance;
    private Date salesDate;
    private Timestamp salesTime;
    private int salesQty;
    private String status;
    private BigDecimal discount;
    private String invoiceNumber;
    private CashBookDto cashBookDto;
    private BankBookDto bankBookDto;

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public int getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(int searchValue) {
        this.searchValue = searchValue;
    }

    public String getSalesRefName() {
        return salesRefName;
    }

    public void setSalesRefName(String salesRefName) {
        this.salesRefName = salesRefName;
    }

    public String getSalesNo() {
        return salesNo;
    }

    public void setSalesNo(String salesNo) {
        this.salesNo = salesNo;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    

    public String getSalesType() {
        return salesType;
    }

    public void setSalesType(String salesType) {
        this.salesType = salesType;
    }

    public Date getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Date salesDate) {
        this.salesDate = salesDate;
    }

    public Timestamp getSalesTime() {
        return salesTime;
    }

    public void setSalesTime(Timestamp salesTime) {
        this.salesTime = salesTime;
    }

    public int getSalesQty() {
        return salesQty;
    }

    public void setSalesQty(int salesQty) {
        this.salesQty = salesQty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public CashBookDto getCashBookDto() {
        return cashBookDto;
    }

    public void setCashBookDto(CashBookDto cashBookDto) {
        this.cashBookDto = cashBookDto;
    }

    public BankBookDto getBankBookDto() {
        return bankBookDto;
    }

    public void setBankBookDto(BankBookDto bankBookDto) {
        this.bankBookDto = bankBookDto;
    }

    public String getSaleItems() {
        return saleItems;
    }

    public void setSaleItems(String saleItems) {
        this.saleItems = saleItems;
    }

    public BigDecimal getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(BigDecimal invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }

    public BigDecimal getInvoiceBalance() {
        return invoiceBalance;
    }

    public void setInvoiceBalance(BigDecimal invoiceBalance) {
        this.invoiceBalance = invoiceBalance;
    }
}