/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class MaterialDto {
    private int materialId; 
    private int supplierId; 
    private String supplierName; 
    private String userName; 
    private String materialName; 
    private String materialType; 
    private BigDecimal materialQty; 
    private BigDecimal materialIn; 
    private BigDecimal materialOut; 
    private BigDecimal materialPrice; 
    private String materialReason; 
    private Date materialDate; 
    private BigDecimal materialBalance; 
    private String status; 
    
    public MaterialDto() { } 

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public BigDecimal getMaterialQty() {
        return materialQty;
    }

    public void setMaterialQty(BigDecimal materialQty) {
        this.materialQty = materialQty;
    }

    public BigDecimal getMaterialIn() {
        return materialIn;
    }

    public void setMaterialIn(BigDecimal materialIn) {
        this.materialIn = materialIn;
    }

    public BigDecimal getMaterialOut() {
        return materialOut;
    }

    public void setMaterialOut(BigDecimal materialOut) {
        this.materialOut = materialOut;
    }

    public BigDecimal getMaterialPrice() {
        return materialPrice;
    }

    public void setMaterialPrice(BigDecimal materialPrice) {
        this.materialPrice = materialPrice;
    }

    public String getMaterialReason() {
        return materialReason;
    }

    public void setMaterialReason(String materialReason) {
        this.materialReason = materialReason;
    }

    public Date getMaterialDate() {
        return materialDate;
    }

    public void setMaterialDate(Date materialDate) {
        this.materialDate = materialDate;
    }

    public BigDecimal getMaterialBalance() {
        return materialBalance;
    }

    public void setMaterialBalance(BigDecimal materialBalance) {
        this.materialBalance = materialBalance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MaterialDto{" + "materialId=" + materialId + ", supplierId=" + supplierId + ", supplierName=" + supplierName + ", userName=" + userName + ", materialName=" + materialName + ", materialType=" + materialType + ", materialQty=" + materialQty + ", materialIn=" + materialIn + ", materialOut=" + materialOut + ", materialPrice=" + materialPrice + ", materialReason=" + materialReason + ", materialDate=" + materialDate + ", materialBalance=" + materialBalance + ", status=" + status + '}';
    }   
}
