/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class MaterialOutDto {
    private int materialOutId;
    private int materialId;
    private BigDecimal materialOutQty;
    private BigDecimal materialOutPrice;
    private Date materialOutDate;
    private int materialOutMonth;
    private int materialOutYear;
    private Timestamp materialOutTime;
    private BigDecimal materialOutBalance;
    private BigDecimal materialOutTotalCost;
    private String materialOutReason;
    private String materialType;
    private String materialName;

    public int getMaterialOutId() {
        return materialOutId;
    }

    public void setMaterialOutId(int materialOutId) {
        this.materialOutId = materialOutId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public BigDecimal getMaterialOutQty() {
        return materialOutQty;
    }

    public void setMaterialOutQty(BigDecimal materialOutQty) {
        this.materialOutQty = materialOutQty;
    }

    public BigDecimal getMaterialOutPrice() {
        return materialOutPrice;
    }

    public void setMaterialOutPrice(BigDecimal materialOutPrice) {
        this.materialOutPrice = materialOutPrice;
    }

    public Date getMaterialOutDate() {
        return materialOutDate;
    }

    public void setMaterialOutDate(Date materialOutDate) {
        this.materialOutDate = materialOutDate;
    }

    public int getMaterialOutMonth() {
        return materialOutMonth;
    }

    public void setMaterialOutMonth(int materialOutMonth) {
        this.materialOutMonth = materialOutMonth;
    }

    public int getMaterialOutYear() {
        return materialOutYear;
    }

    public void setMaterialOutYear(int materialOutYear) {
        this.materialOutYear = materialOutYear;
    }

    public Timestamp getMaterialOutTime() {
        return materialOutTime;
    }

    public void setMaterialOutTime(Timestamp materialOutTime) {
        this.materialOutTime = materialOutTime;
    }

    public BigDecimal getMaterialOutBalance() {
        return materialOutBalance;
    }

    public void setMaterialOutBalance(BigDecimal materialOutBalance) {
        this.materialOutBalance = materialOutBalance;
    }

    public BigDecimal getMaterialOutTotalCost() {
        return materialOutTotalCost;
    }

    public void setMaterialOutTotalCost(BigDecimal materialOutTotalCost) {
        this.materialOutTotalCost = materialOutTotalCost;
    }

    public String getMaterialOutReason() {
        return materialOutReason;
    }

    public void setMaterialOutReason(String materialOutReason) {
        this.materialOutReason = materialOutReason;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    @Override
    public String toString() {
        return "MaterialOutDto{" + "materialOutId=" + materialOutId + ", materialId=" + materialId + ", materialOutQty=" + materialOutQty + ", materialOutPrice=" + materialOutPrice + ", materialOutDate=" + materialOutDate + ", materialOutMonth=" + materialOutMonth + ", materialOutYear=" + materialOutYear + ", materialOutTime=" + materialOutTime + ", materialOutBalance=" + materialOutBalance + ", materialOutTotalCost=" + materialOutTotalCost + ", materialOutReason=" + materialOutReason + ", materialType=" + materialType + ", materialName=" + materialName + '}';
    }
}
