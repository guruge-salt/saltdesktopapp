/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class ItemDto {

    private int itemId;
    private String userName;
    private String itemName;
    private BigDecimal unitPrice;
    private BigDecimal costPrice;
    private BigDecimal retailPrice;
    private BigDecimal wholeSalePrice;
    private int quantity;
    private BigDecimal totalPrice;
    private BigDecimal discount;
    private String itemStatus;
    private Date itemDate;

    public Date getItemDate() {
        return itemDate;
    }

    public void setItemDate(Date itemDate) {
        this.itemDate = itemDate;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    public ItemDto() {

    }

    public ItemDto(ItemDto selectedDto) {
        itemId = selectedDto.getItemId();
        userName = selectedDto.getUserName();
        itemName = selectedDto.getItemName();
        unitPrice = selectedDto.getUnitPrice();
        costPrice = selectedDto.getCostPrice();
        retailPrice = selectedDto.getRetailPrice();
        wholeSalePrice = selectedDto.getWholeSalePrice();
        quantity = selectedDto.getQuantity();
        totalPrice = selectedDto.getTotalPrice();
        discount = selectedDto.getDiscount();
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public BigDecimal getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(BigDecimal retailPrice) {
        this.retailPrice = retailPrice;
    }

    public BigDecimal getWholeSalePrice() {
        return wholeSalePrice;
    }

    public void setWholeSalePrice(BigDecimal wholeSalePrice) {
        this.wholeSalePrice = wholeSalePrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
}
