/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class MaterialInDto {
    private int materialInId;
    private int materialId;
    private BigDecimal materialInQty;
    private BigDecimal materialInPrice;
    private Date materialInDate;
    private int materialInMonth;
    private int materialInYear;
    private Timestamp materialInTime;
    private BigDecimal materialInBalance;
    private BigDecimal materialInTotalCost;
    private String materialInReason;
    private String materialType;
    private String materialName;

    public int getMaterialInId() {
        return materialInId;
    }

    public void setMaterialInId(int materialInId) {
        this.materialInId = materialInId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public BigDecimal getMaterialInQty() {
        return materialInQty;
    }

    public void setMaterialInQty(BigDecimal materialInQty) {
        this.materialInQty = materialInQty;
    }

    public BigDecimal getMaterialInPrice() {
        return materialInPrice;
    }

    public void setMaterialInPrice(BigDecimal materialInPrice) {
        this.materialInPrice = materialInPrice;
    }

    public Date getMaterialInDate() {
        return materialInDate;
    }

    public void setMaterialInDate(Date materialInDate) {
        this.materialInDate = materialInDate;
    }

    public int getMaterialInMonth() {
        return materialInMonth;
    }

    public void setMaterialInMonth(int materialInMonth) {
        this.materialInMonth = materialInMonth;
    }

    public int getMaterialInYear() {
        return materialInYear;
    }

    public void setMaterialInYear(int materialInYear) {
        this.materialInYear = materialInYear;
    }

    public Timestamp getMaterialInTime() {
        return materialInTime;
    }

    public void setMaterialInTime(Timestamp materialInTime) {
        this.materialInTime = materialInTime;
    }

    public BigDecimal getMaterialInBalance() {
        return materialInBalance;
    }

    public void setMaterialInBalance(BigDecimal materialInBalance) {
        this.materialInBalance = materialInBalance;
    }

    public BigDecimal getMaterialInTotalCost() {
        return materialInTotalCost;
    }

    public void setMaterialInTotalCost(BigDecimal materialInTotalCost) {
        this.materialInTotalCost = materialInTotalCost;
    }

    public String getMaterialInReason() {
        return materialInReason;
    }

    public void setMaterialInReason(String materialInReason) {
        this.materialInReason = materialInReason;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }
}
