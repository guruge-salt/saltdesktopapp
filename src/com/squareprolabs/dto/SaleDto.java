/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class SaleDto {
    private int saleId;
    private String userName;
    private String salesNo;
    private int customerId;
    private List<ItemDto> itemDtos;
    private String salesType;
    private Date salesDate;
    private Timestamp salesTime;
    private int salesQty;
    private BigDecimal discount;
    private InvoiceDto invoiceDto;
    private CashBookDto cashBookDto;
    private BankBookDto bankBookDto;
    private String result;

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSalesNo() {
        return salesNo;
    }

    public void setSalesNo(String salesNo) {
        this.salesNo = salesNo;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public List<ItemDto> getItemDtos() {
        return itemDtos;
    }

    public void setItemDtos(List<ItemDto> itemDtos) {
        this.itemDtos = itemDtos;
    }

    public String getSalesType() {
        return salesType;
    }

    public void setSalesType(String salesType) {
        this.salesType = salesType;
    }

    public Date getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Date salesDate) {
        this.salesDate = salesDate;
    }

    public Timestamp getSalesTime() {
        return salesTime;
    }

    public void setSalesTime(Timestamp salesTime) {
        this.salesTime = salesTime;
    }

    public int getSalesQty() {
        return salesQty;
    }

    public void setSalesQty(int salesQty) {
        this.salesQty = salesQty;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public InvoiceDto getInvoiceDto() {
        return invoiceDto;
    }

    public void setInvoiceDto(InvoiceDto invoiceDto) {
        this.invoiceDto = invoiceDto;
    }

    public CashBookDto getCashBookDto() {
        return cashBookDto;
    }

    public void setCashBookDto(CashBookDto cashDto) {
        this.cashBookDto = cashDto;
    }

    public BankBookDto getBankBookDto() {
        return bankBookDto;
    }

    public void setBankBookDto(BankBookDto bankBookDto) {
        this.bankBookDto = bankBookDto;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
        
}
