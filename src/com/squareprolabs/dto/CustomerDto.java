/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.dto;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class CustomerDto {
    private int customerId;
    private String user;
    private String customerName;
    private String phoneNumber;
    private String email;
    private String nic;
    private String customerAddress;
    private String customerStatus;
    private String status;
    private int userId;
    private String customerType;
    
    public CustomerDto(){
        
    }
    
    public CustomerDto(CustomerDto dto) {
        customerId = dto.getCustomerId();
        user = dto.getUser();
        customerName = dto.getCustomerName();
        phoneNumber = dto.getPhoneNumber();
        email = dto.getEmail();
        nic = dto.getNic();
        customerAddress = dto.getCustomerAddress();
        customerStatus = dto.getCustomerStatus();
        status = dto.getStatus();
        userId = dto.getUserId();
        customerType = dto.getCustomerType();
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }
}
