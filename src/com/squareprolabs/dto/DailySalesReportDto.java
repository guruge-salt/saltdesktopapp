/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.dto;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class DailySalesReportDto {
    private Map<String, DailySalesReportItemDto> dailySalesByAgent;

    public Map<String, DailySalesReportItemDto> getDailySalesByAgent() {
        return dailySalesByAgent;
    }
 
    
    
    public void setDailySalesByAgent(Map<String, DailySalesReportItemDto> dailySalesByAgent) {
        this.dailySalesByAgent = dailySalesByAgent;
    }
}
