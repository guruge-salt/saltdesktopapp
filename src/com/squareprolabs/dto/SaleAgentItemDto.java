/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.squareprolabs.dto;

import java.util.Date;

/**
 *
 * @author Yachitha Sandaruwan
 */
public class SaleAgentItemDto {
    private Date agentStockDate;
    private int id;
    private int searchValue;

    public Date getAgentStockDate() {
        return agentStockDate;
    }

    public void setAgentStockDate(Date agentStockDate) {
        this.agentStockDate = agentStockDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(int searchValue) {
        this.searchValue = searchValue;
    }
}
